package com.example.igor.plantssm;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;

public class AllPlantInformation extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    Plant plant;
    ImageView imageView;
    TextView nameTextView;
    TextView describeTextView;
    MemoryManager mm;
    MainActivity mContext;


    //Obsluga dodania zadania
    TextInputEditText searchEditText;
    Button confirmAddTaskButton;

    Task newTask;


    //Obsluga wyświetlenia zadania
    Button confirmTask;
    TextView nextTermInfoForUser;
    TextView nextTaskTerm;
    Boolean isTaskChanged = false;
    public static final String EXTRA_RETURN_KEY = "EXTRA_KEY";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_plant_information);
        mContext = new MainActivity();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        //Memory
        mm= new MemoryManager();

        //Pobranie obiektu
        Intent i = getIntent();
        plant = (Plant) i.getSerializableExtra("Object");


        /*****Inicjalizacja zmiennych obsługujących View
         * ********************************************
         * ********************************************
         */
        //Zdjęcie rośliny
        imageView =  findViewById(R.id.imageViewPlantInformation);
        //Nazwa rośliny
        nameTextView = findViewById(R.id.nameTextViewPlantInformation);
        //OPis rośliny
        describeTextView = findViewById(R.id.descibeTextViewPlantInformation);
        //Pole tekstowe zawierające opis rośliny
        searchEditText = findViewById(R.id.taskDescribeInput);
        //Button dodający task
        confirmAddTaskButton = findViewById(R.id.addTaskButton);
        //Button potwierdzający zrealizowanie tasku
        confirmTask = findViewById(R.id.doneTaskButton);
        //termin tasku
        nextTaskTerm = findViewById(R.id.nextTermTextView);


        /***Ustawienie Views na wartości obiektu
         * *************************************
         * ************************************
         */
        //Pobranie obrazu rośliny i ustawienie jeśli istnieje
        Bitmap b  =mm.loadBitmapFromStorage(plant.getFileName(), plant.getName());
        if(b!=null)
            imageView.setImageBitmap(b);
        nameTextView.setText(plant.getName());
        describeTextView.setText(plant.getDescription());
        describeTextView.setMovementMethod(new ScrollingMovementMethod());

        /***Task może być nullem a więc trzeba sprawdzić czy istnieje
         * **********************************************************
         */
        if(plant.getTask()!= null){
            //Usunięcie kursoru który wraca w momencie najechania na TextInputEditText
            searchEditText.setFocusable(false);
            searchEditText.setHint(plant.getTask().getDescribe());

            //getmMonth()+1 ponieważ dla Obiektu przechowującego Datę Styczeń - 0 miesiac, Grudzień - 11 miesiąć
            String date = plant.getTask().getmYear() + "  " + (plant.getTask().getmMonth()+1) + "  " + plant.getTask().getmDay();
            nextTaskTerm.setText(date);
        }else{
            searchEditText.setHint("Brak zadania");
            nextTaskTerm.setText("Kliknij");
            confirmTask.setVisibility(View.INVISIBLE);

        }

        //W razie wciśnięcia textView z terminem
        nextTaskTerm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DatePickerFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(),"date picker");
            }


        });

        //W razie wciśnięcia Buttona Zaktualizuj
        confirmAddTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newTask.setDescribe(searchEditText.getText().toString());
                plant.setTask(newTask);

                searchEditText.setText(newTask.getDescribe());

                String date = newTask.getmYear() + "  " + (newTask.getmMonth()+1) + "  " + newTask.getmDay();
                nextTaskTerm.setText(date);

                isTaskChanged = true;

                Toast toast = Toast.makeText(getApplicationContext(), "Zaktualizowano zadanie", Toast.LENGTH_SHORT);
                toast.show();

            }

        });

        //Przycisk potwierdzające realizację zadania
        confirmTask.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                plant.setTask(null);
                isTaskChanged = true;

                searchEditText.setHint("Brak zadania");
                nextTaskTerm.setText("Kliknij");
                Toast toast = Toast.makeText(getApplicationContext(), "Zadanie zakończone", Toast.LENGTH_SHORT);
                toast.show();
                }
        });

        //W razie próby wpisania nowego opisu tasku
        searchEditText.setOnTouchListener(new View.OnTouchListener(){
            public boolean onTouch(View v, MotionEvent event) {

                searchEditText.setFocusableInTouchMode(true);

                return false;
            }



        });


    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        newTask = new Task(year,month,dayOfMonth);
    }
    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent(this,MainActivity.class);
        returnIntent.putExtra(EXTRA_RETURN_KEY,plant);

        if(isTaskChanged==true){
            setResult(RESULT_OK,returnIntent);
            finish();
        }else{
            setResult(RESULT_CANCELED,returnIntent);
            finish();
        }


    }

    }


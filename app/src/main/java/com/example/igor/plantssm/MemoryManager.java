package com.example.igor.plantssm;
import android.util.Log;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class MemoryManager {
    //Klasa do obsługi pamięci
    private String KEY_CONNECTIONS = "NEW2";
    void saveListOfPlantsToMemory(Activity activity, ArrayList<Plant> list)
    {
        SharedPreferences.Editor editor = activity.getPreferences(MODE_PRIVATE).edit();
        String connectionsJSONString = new Gson().toJson(list);
        editor.putString(KEY_CONNECTIONS, connectionsJSONString);
        editor.apply();
    }
    ArrayList<Plant> readListOfPlantsFromMemory(Activity activity)
    {
        Type type = new TypeToken< List < Plant >>() {}.getType();

        String connectionsJSONString = activity.getPreferences(MODE_PRIVATE).getString(KEY_CONNECTIONS, null);
        ArrayList <Plant> listOfPlants = new Gson().fromJson(connectionsJSONString, type);

        if(listOfPlants != null)
            return listOfPlants;
        else
            return new ArrayList<Plant>();
    }

    String saveBitmapToStorage(Bitmap bitmap, String fileName,Context context) {
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File myPath = new File(directory, fileName+".png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myPath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("saveBitmapToStorage","2");

        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("saveBitmapToStorage","3");


            }
        }
        return directory.getAbsolutePath();
    }
    Bitmap loadBitmapFromStorage(String path, String fileName) {
        try{
            File f=new File(path, "/"+fileName+".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
        }catch(FileNotFoundException e)
        {
            e.printStackTrace();
            Log.e("loadBitmapFromStorage","3");

            return null;
        }
    }

}

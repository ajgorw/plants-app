package com.example.igor.plantssm;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.Serializable;


public class UserInput extends AppCompatActivity implements Serializable {


    public static String KEY_NEW_PLANT_OBJ = "NEW_PLANT_OBJ";
    public static int IMAGE_CAPTURE_CODE = 1;
    TextInputEditText nameField;
    TextInputEditText descriptionField;
    MemoryManager mg;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_input);

        mg = new MemoryManager();

        nameField =  findViewById(R.id.nameInput);
        descriptionField =  findViewById(R.id.descriptionInput);


        //Obsługa kamery
        Button cameraButtton =  findViewById(R.id.makePhotoButton);

        cameraButtton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,IMAGE_CAPTURE_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == IMAGE_CAPTURE_CODE)
        {
            if(resultCode == RESULT_OK)
            {
                bitmap = (Bitmap) data.getExtras().get("data");

            }
        }
    }
    public void makeObject(View v)
    {

        Plant newPlant = new Plant(nameField.getText().toString());
        newPlant.setDescription(descriptionField.getText().toString());

        newPlant.setFileName(mg.saveBitmapToStorage(bitmap,newPlant.getName(),getApplicationContext()));
        Log.e("makeObject()",newPlant.getFileName());


        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(KEY_NEW_PLANT_OBJ,newPlant);
        startActivity(i);
        finish();

    }
}

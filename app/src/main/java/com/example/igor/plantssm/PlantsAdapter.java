package com.example.igor.plantssm;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PlantsAdapter extends RecyclerView.Adapter   {
    // źródło danych
    private List<Plant> mPlants = new ArrayList<>();
    private MainActivity context;
    public static final int PLANT_INFO_REQUEST = 1;
    // obiekt listy artykułów
    private RecyclerView mRecyclerView;

    // implementacja wzorca ViewHolder
    // każdy obiekt tej klasy przechowuje odniesienie do layoutu elementu listy
    // dzięki temu wywołujemy findViewById() tylko raz dla każdego elementu
    private class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImage;
        public TextView mTitle;
        public ImageView taskStatus;

        public MyViewHolder(View pItem) {
            super(pItem);
            mTitle = (TextView) pItem.findViewById(R.id.itemTextView);
            mImage = (ImageView) pItem.findViewById(R.id.itemImageView);
            taskStatus = (ImageView) pItem.findViewById(R.id.taskStatus);
        }
    }

    // konstruktor adaptera
    public PlantsAdapter(List<Plant> pPlants, RecyclerView pRecyclerView, MainActivity context) {
        mPlants = pPlants;
        mRecyclerView = pRecyclerView;
        this.context = context;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        // tworzymy layout artykułu oraz obiekt ViewHoldera
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_plant, viewGroup, false);

        // dla elementu listy ustawiamy obiekt OnClickListener,
        // który usunie element z listy po kliknięciu na niego
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if(context.viewOrDeletePlantButton.isChecked())
               {
                   int position = mRecyclerView.getChildAdapterPosition(v);
                   context.plantsList.remove(mPlants.get(position));
                   mPlants.remove(position);
                   notifyItemRemoved(position);
                   notifyItemRangeChanged(position,mPlants.size());
                   context.memoryManager.saveListOfPlantsToMemory(context,context.plantsList);

               }
               else {
                   // odnajdujemy indeks klikniętego elementu
                   int position = mRecyclerView.getChildAdapterPosition(v);
                   Intent i = new Intent(context, AllPlantInformation.class);
                   i.putExtra("Object", mPlants.get(position));
                   context.startActivityForResult(i,PLANT_INFO_REQUEST);
               }

            }
        });
        // tworzymy i zwracamy obiekt ViewHolder
        return new MyViewHolder(view);
    }


    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int i) {
        // uzupełniamy layout artykułu
        Plant plant = mPlants.get(i);
        MemoryManager mm = new MemoryManager();
        Bitmap b = mm.loadBitmapFromStorage(plant.getFileName(), plant.getName());

        ((MyViewHolder) viewHolder).mTitle.setText(plant.getName());
        ((MyViewHolder) viewHolder).mImage.setImageBitmap(b);


        Task task = plant.getTask();
        if(task!=null) {
            if (task.getTaskStatus()==1) {
                Log.e("1","jest 1");
                ((MyViewHolder) viewHolder).taskStatus.setImageResource(R.drawable.donetask);
            } else if (task.getTaskStatus() == 0) {
                Log.e("2","jest 2");

                ((MyViewHolder) viewHolder).taskStatus.setImageResource(R.drawable.taskbad);
            }
            else if(task.getTaskStatus() ==2)
                ((MyViewHolder) viewHolder).taskStatus.setImageResource(R.drawable.waiting);


        }
        else
            ((MyViewHolder) viewHolder).taskStatus.setImageResource(R.drawable.donetask);

    }

    public int getItemCount() {
        return mPlants.size();
    }
    public  void  filterList(ArrayList<Plant> filteredList){
        mPlants = filteredList;
        notifyDataSetChanged();

    }





}

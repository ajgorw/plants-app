package com.example.igor.plantssm;

import android.util.Log;

import java.io.Serializable;
import java.util.Calendar;

public class Task implements Serializable{
    public int getHour() {
        return hour;
    }
    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getmYear() {
        return mYear;
    }

    public void setmYear(int mYear) {
        this.mYear = mYear;
    }

    public int getmMonth() {
        return mMonth;
    }

    public void setmMonth(int mMonth) {
        this.mMonth = mMonth;
    }

    public int getmDay() {
        return mDay;
    }

    public void setmDay(int mDay) {
        this.mDay = mDay;
    }

    public int getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(int periodicity) {
        this.periodicity = periodicity;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    int hour;
    int mYear;
    int mMonth;
    int mDay;
    int periodicity;

    public Boolean getTaskDone() {
        return isTaskDone;
    }

    public void setTaskDone(Boolean taskDone) {
        isTaskDone = taskDone;
    }

    Boolean isTaskDone;
    String describe;
    public Task(int y, int m, int d)
    {
        setmYear(y);
        setmMonth(m);
        setmDay(d);
        setHour(12);
        isTaskDone = false;
    }

    public int getTaskStatus(){
        Calendar dateFromTask = Calendar.getInstance();
        dateFromTask.set(Calendar.YEAR,this.getmYear());
        dateFromTask.set(Calendar.MONTH,this.getmMonth());
        dateFromTask.set(Calendar.DAY_OF_MONTH,this.getmDay());

        //Na sztywno ustawienie godziny przypomnienia
        dateFromTask.set(Calendar.HOUR,5);
        Calendar actuallDate = Calendar.getInstance();


        //Jeżeli task nie został wykonany i jest po terminie zwróc red image
        if(dateFromTask.compareTo(actuallDate) < 0 && isTaskDone==false)
            return 0;
        //Jeżeli task został wykonany i jest po terminie
        else if(dateFromTask.compareTo(actuallDate) < 0 && isTaskDone == true)
            return 1;
        //Jeżeli task jest oczekujący
        else if(dateFromTask.compareTo(actuallDate) > 0)
            return 2;
        else
            return 3;

    }

}

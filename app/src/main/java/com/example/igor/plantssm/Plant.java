package com.example.igor.plantssm;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Plant implements Serializable {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    private Bitmap Picutre;
    private String fileName;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    private Task task;



    public void setFileName(String b) {
        this.fileName = b;
    }
    public String getFileName() {
        return this.fileName;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;
    public Plant(String n)
    {
        setName(n);
    }


}

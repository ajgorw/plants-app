package com.example.igor.plantssm;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
public class MainActivity extends AppCompatActivity implements Serializable {
    PlantsAdapter plantsAdapter;
    public MemoryManager memoryManager;
    RecyclerView recyclerView;

    //Główna lista do której wczytuje i odczytuje wszystkie obiekty Plant
    public static ArrayList <Plant> plantsList;
    Button addPlantBt;
    //Obsluga pogody
    TextView cityField;
    TextView  day1Date, day2Date, day3Date, day4Date, day5Date, day6Date;
    TextView day1Temperature,day2Temperature,day3Temperature,day4Temperature,day5Temperature,day6Temperature;
    String newCity = "";
    String country = ",PL";


    String city = "Serpelice, PL";
    String OPEN_WEATHER_MAP_API = "4f9c9c4c41603e0fcaad011e8b82521a";
    public Switch viewOrDeletePlantButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        day1Date = findViewById(R.id.day1Date);
        day2Date = findViewById(R.id.day2Date);
        day3Date = findViewById(R.id.day3Date);
        day4Date = findViewById(R.id.day4Date);
        day5Date = findViewById(R.id.day5Date);
        day6Date = findViewById(R.id.day6Date);

        day1Temperature = findViewById(R.id.day1Temperature);
        day2Temperature = findViewById(R.id.day2Temperature);
        day3Temperature = findViewById(R.id.day3Temperature);
        day4Temperature = findViewById(R.id.day4Temperature);
        day5Temperature = findViewById(R.id.day5Temperature);
        day6Temperature = findViewById(R.id.day6Temperature);



        //MemmoryManager odpowiada za wszystkie operacje zwiazane
        //z zapisywaniem i odczytywaniem obiektow do pamieci
        memoryManager = new MemoryManager();

        //Próba wczytania i inicjalizacji list<Plant>
        //W razie niepowodzenia utworzenie pustej listy
        plantsList = memoryManager.readListOfPlantsFromMemory(this);
        //plantsList = new ArrayList<>();
        Log.e("plantsList in Main",Integer.toString(plantsList.size()));
        Intent i = getIntent();

        Plant newPlant = null;
        Bundle extras = i.getExtras();
        if(extras != null)
            newPlant = (Plant)getIntent().getSerializableExtra(UserInput.KEY_NEW_PLANT_OBJ);

        Bitmap nowa = null;
        if(plantsList != null && newPlant == null && plantsList.size()!=0)
        {
            int lastElement = plantsList.size() - 1;
            Bitmap b = memoryManager.loadBitmapFromStorage(plantsList.get(lastElement).getFileName(),plantsList.get(lastElement).getName());
        }
        if (plantsList != null && newPlant != null) {
            plantsList.add(newPlant);
            Bitmap b = memoryManager.loadBitmapFromStorage(newPlant.getFileName(),newPlant.getName());
            memoryManager.saveListOfPlantsToMemory(this, plantsList);

            newPlant = null;
            nowa = null;
        }

        //Obsługa RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.rvPlants);
        // w celach optymalizacji
        recyclerView.setHasFixedSize(true);

        // ustawiamy LayoutManagera
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // ustawiamy animatora, który odpowiada za animację dodania/usunięcia elementów listy
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        // tworzymy adapter oraz łączymy go z RecyclerView
        if(plantsList.size()!=0) {
            plantsAdapter = new PlantsAdapter(plantsList, recyclerView, this);
            recyclerView.setAdapter(plantsAdapter);
        }

        //Inicjalizacja zmiennych do obslugi pogody
        cityField = (TextView) findViewById(R.id.city_field);
        //currentTemperatureField = (TextView) findViewById(R.id.current_temperature_field);
        //weatherIcon = (TextView) findViewById(R.id.weather_icon);
        addPlantBt = (Button) findViewById(R.id.addPlantButton);

        taskLoadUp(city);


        addPlantBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), UserInput.class);
                startActivity(intent);
                Log.e("Rozmiar listy",String.valueOf(plantsList.size()));


            }
        });

        //Search functionality
        TextInputEditText searchEditText = findViewById(R.id.filterInput);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        viewOrDeletePlantButton = findViewById(R.id.switch1);
        viewOrDeletePlantButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Toast toast = Toast.makeText(getApplicationContext(), "Tryb usuwania roślin", Toast.LENGTH_SHORT);
                    toast.show();
                }
                else
                {
                    Toast toast = Toast.makeText(getApplicationContext(), "Tryb przeglądania roślin", Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                showInputDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //New localization Dialog
    protected void showInputDialog() {

        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText =  promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        newCity= editText.getText().toString();
                        cityField.setText(newCity);
                        taskLoadUp(newCity+country);
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    //Obsluga menu w ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    //Funkcja filtrujaca liste na podstawie tekstu uzytkownika
    private void filter(String text){
        ArrayList<Plant> filteredList = new ArrayList<>();
        for(Plant item : plantsList){
            if(item.getName().toLowerCase().contains(text.toLowerCase())){
                filteredList.add(item);
            }
        }
        plantsAdapter.filterList(filteredList);

    }

    public void taskLoadUp(String query) {
        if (Function.isNetworkAvailable(getApplicationContext())) {
            DownloadWeather task = new DownloadWeather();
            task.execute(query);
        } else {
            Toast.makeText(getApplicationContext(), R.string.noInternetConnections, Toast.LENGTH_LONG).show();
        }
    }



    class DownloadWeather extends AsyncTask< String, Void, String > {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        protected String doInBackground(String...args) {
            String xml = Function.excuteGet("http://api.openweathermap.org/data/2.5/forecast?q=" + args[0] +
                    "&units=metric&appid=" + OPEN_WEATHER_MAP_API);

            return xml;
        }
        @Override
        protected void onPostExecute(String xml) {
            Date newDate = new Date();
            DateTime today = new DateTime(newDate);
            SimpleDateFormat simpleDateformat = new SimpleDateFormat("E");


            try {
                JSONObject json = new JSONObject(xml);
                if (json != null) {
                    JSONObject details = json.getJSONArray("list").getJSONObject(1);

                    JSONArray arr = json.getJSONArray("list");
                    Log.e("Rozmiar array", Integer.toString(arr.length()));

                    int n = 0;
                    for(int i = 0 ; i < arr.length();i+=8){
                        JSONObject obj = arr.getJSONObject(i);

                        JSONObject mainFromObj = obj.getJSONObject("main");

                        double temp = mainFromObj.getDouble("temp");
                        String objDate = obj.getString("dt_txt");
                        Log.e("Temp",  Double.toString(temp));
                        Log.e("Data",  objDate);

                        String [] date = objDate.split(" ",4);
                        if(n==0){
                            day1Date.setText(simpleDateformat.format(today.toDate()));
                            day1Temperature.setText( (Double.toString(temp) )+ "°C" );
                        }
                        else if(n==1){
                            DateTime plus = today.plusDays(1);
                            day2Date.setText(simpleDateformat.format(plus.toDate()));
                            day2Temperature.setText(Double.toString(temp) + "°C");
                        }
                        else if(n==2){
                            DateTime plus = today.plusDays(2);
                            day3Date.setText(simpleDateformat.format(plus.toDate()));
                            day3Temperature.setText(Double.toString(temp) + "°C");
                        }
                        else if(n==3){
                            DateTime plus = today.plusDays(3);
                            day4Date.setText(simpleDateformat.format(plus.toDate()));
                            day4Temperature.setText(Double.toString(temp) + "°C");
                        }
                        else if(n==4){
                            DateTime plus = today.plusDays(4);
                            day5Date.setText(simpleDateformat.format(plus.toDate()));
                            day5Temperature.setText(Double.toString(temp) + "°C");
                        }
                        n++;

                    }
                    //Dostęp do sotatniego obiektu
                    JSONObject obj = arr.getJSONObject(arr.length()-1);

                    JSONObject mainFromObj = obj.getJSONObject("main");

                    double temp = mainFromObj.getDouble("temp");
                    String objDate = obj.getString("dt_txt");
                    Log.e("Temp",  Double.toString(temp));
                    Log.e("Data",  objDate);
                    String [] date = objDate.split(" ",4);

                    DateTime plus = today.plusDays(5);
                    day6Date.setText(simpleDateformat.format(plus.toDate()));
                    day6Temperature.setText(Double.toString(temp) + "°C");


                }
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(), "Error, Check City", Toast.LENGTH_SHORT).show();
            }


        }



    }


    public void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PlantsAdapter.PLANT_INFO_REQUEST && data!=null) {
            Log.e("requestCode","requestCode");
            //W razie powrotu z Activity wyświetlającego szczegóły rośliny
            if (resultCode == RESULT_OK) {
                Log.e("RESULT_OK","RESULT OK");
                //Intent intent = getIntent();
                Plant p = (Plant)data.getSerializableExtra(AllPlantInformation.EXTRA_RETURN_KEY);
                //Jeśli task nie jest nullem należy zaktualizować obiekt Plant w liście
                if(p.getTask() != null){
                    for(int i = 0 ; i < plantsList.size() ;i++){
                        Plant tmp = plantsList.get(i);
                        if(p.getName().equals(tmp.getName())){
                            plantsList.set(i,p);
                            plantsAdapter.notifyDataSetChanged();

                            memoryManager.saveListOfPlantsToMemory(this,plantsList);
                            break;
                        }
                    }
                }else{
                    for(int i = 0 ; i < plantsList.size() ;i++){
                        Plant tmp = plantsList.get(i);
                        if(p.getName().equals(tmp.getName())){
                            plantsList.set(i,p);
                            memoryManager.saveListOfPlantsToMemory(this,plantsList);
                            plantsAdapter.notifyDataSetChanged();
                        }
                    }

                }
            }else{
                Log.e("RESULT_OK else","else");

            }
        }
    }
}
